package com.example.adapter;

import com.example.advisorysystem.Inbox;
import com.example.advisorysystem.Outbox;
import com.example.advisorysystem.Sent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

/*
 * Author-UB
 * TabsPagerAdapter holds the fragments for text activity.
 * 
*/
public class TabsPagerAdapter extends FragmentStatePagerAdapter{


	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
		
	}
	@Override
	public Fragment getItem(int index) {
		// TODO Auto-generated method stub
		switch (index) {
		case 0:
			return new Inbox();
		case 1:
			return new Outbox();
		case 2:
			return new Sent();
			
		}
		return null;
	}

	@Override
	public int getCount() {
		
		return 3;	//important to set count
	}

}