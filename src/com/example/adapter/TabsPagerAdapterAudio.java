package com.example.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.advisorysystem.AudioInbox;
import com.example.advisorysystem.AudioOutbox;
import com.example.advisorysystem.AudioSent;
import com.example.advisorysystem.Inbox;
import com.example.advisorysystem.Outbox;
import com.example.advisorysystem.Sent;

/*
 * Author-UB
 * TabsPagerAdapterAudio holds the fragments for Audio activity.
 * 
 */

public class TabsPagerAdapterAudio extends FragmentStatePagerAdapter{


	public TabsPagerAdapterAudio(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		// TODO Auto-generated method stub
		switch (index) {
		case 0:
			return new AudioInbox();

		case 1:
			return new AudioSent();
			//break;
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
}
