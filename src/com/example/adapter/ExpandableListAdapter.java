package com.example.adapter;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.advisorysystem.DashBoard;
import com.example.advisorysystem.PublisherCompose;
import com.example.advisorysystem.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> _listDataChild;
	List<String> phoneNumbers;
	HashMap<Integer, Boolean> mCheckedStates;
	ImageView add;
	boolean isSelected=false;



	public ExpandableListAdapter(Context context, List<String> listDataHeader,
			HashMap<String, List<String>> listDataChild) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listDataChild;
		//phoneNumbers = phoneNum;
		mCheckedStates = new HashMap<Integer,Boolean>();
		for(int i=0 ;i <getGroupCount();i++){
			mCheckedStates.put(i, false);
		}
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}


	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}


	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}


	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		String headerTitle = (String) getGroup(groupPosition);
		final int groupPos = groupPosition;
		final ViewGroup tempParent = parent;
		final View tempConvertView = convertView;
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.expandable_list_group, null);
		}

		if(mCheckedStates.get(groupPos)){
			convertView.setBackgroundColor(Color.YELLOW);
		}else{
			convertView.setBackgroundColor(Color.parseColor("#EDE7F6"));
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.expandable_list_header);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);

		convertView.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(groupPos == 0){
					for(int i =1;i<getGroupCount();i++){
						mCheckedStates.put(i, true);
						View tempView = getGroupView(i, false, tempConvertView, tempParent);
						tempView.setBackgroundColor(Color.YELLOW);
						notifyDataSetChanged();
						for(int j = 1;j<getChildrenCount(i);j++){
							PublisherCompose.PhoneNumbers.add(PublisherCompose.numMapping.get(getChild(i, j).toString()));
						}
					}
				}

				else{
					if(!mCheckedStates.get(groupPos)){
						mCheckedStates.put(groupPos, true);
						v.setBackgroundColor(Color.YELLOW);
						for(int i =0 ; i <getChildrenCount(groupPos);i++){
							PublisherCompose.PhoneNumbers.add(PublisherCompose.numMapping.get(getChild(groupPos, i).toString()));
						}
					}
					else{
						v.setBackgroundColor(Color.parseColor("#EDE7F6"));
						mCheckedStates.put(groupPos, false);
						for(int i =0 ; i <getChildrenCount(groupPos);i++){
							PublisherCompose.PhoneNumbers.remove(PublisherCompose.numMapping.get(getChild(groupPos, i).toString()));
						}
					}
				}
			}
		});

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final String childText = (String) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.expandable_list_item, null);
		}

		final TextView txtListChild = (TextView) convertView
				.findViewById(R.id.expandable_list_item);

		txtListChild.setText(childText);
		return convertView;
	}
}
