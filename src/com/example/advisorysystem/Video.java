package com.example.advisorysystem;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar.Tab;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.adapter.TabsPagerAdapter;

public class Video extends ActionBarActivity {

	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = { "Inbox", "Outbox", "Sent" };


	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.tabslayout);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setTitle("Video Messages");

		viewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);

		ActionBar.TabListener tabListener = new ActionBar.TabListener() {
		

			@Override
			public void onTabReselected(Tab arg0,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTabSelected(Tab tab,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				Log.d("Parag","bug "+ tab.getPosition());
				viewPager.setCurrentItem(tab.getPosition());
				Log.d("Parag","Inside onTabSelected");
			}

			@Override
			public void onTabUnselected(Tab arg0,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub

			}
		};

		for (int i = 0; i < 3; i++) {
			actionBar.addTab(
					actionBar.newTab()
					.setText(tabs[i])
					.setTabListener((android.support.v7.app.ActionBar.TabListener) tabListener));
		}

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {



			@Override
			public void onPageSelected(int position) {
				// When swiping between pages, select the
				// corresponding tab.
				getSupportActionBar().setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id == R.id.home){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}




}
