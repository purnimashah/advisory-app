package com.example.advisorysystem;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.adapter.ExpandableListAdapter;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.text.method.DialerKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.Toast;

public class PublisherCompose extends ActionBarActivity {

	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;
	static List<String> PhoneNamesNumbers;
	public static List<String> PhoneNumbers;
	Button composeGroupMessage, selectAll;
	public static Map <String,String> numMapping;
	List <String> finalNum;
	Map<Integer,Map<Integer,Boolean>> checkStates;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.publisher_compose);
		composeGroupMessage = (Button)findViewById(R.id.ComposeGroupMessage);
	//	selectAll = (Button)findViewById(R.id.selectAll);
		expListView = (ExpandableListView) findViewById(R.id.expand_contacts);
		Map<String,String> tempMap = new HashMap();
		String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/IVRS/MembersList.txt";
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();
		numMapping = new HashMap<String,String>();
		finalNum = new ArrayList<String>();
		PhoneNumbers = new ArrayList<String>();

		try
		{
			FileInputStream instream = new FileInputStream(path); 
			InputStreamReader inputreader = new InputStreamReader(instream); 
			BufferedReader buffreader = new BufferedReader(inputreader); 
			String line = "";
			String tempHeader;
			PhoneNamesNumbers = new ArrayList<String>();
			try
			{	listDataHeader.add("Select All");
				listDataChild.put("Select", PhoneNamesNumbers);
				
				
				while ((line = buffreader.readLine()) != null)
				{	
					PhoneNamesNumbers = new ArrayList<String>();
					tempHeader=line;
					listDataHeader.add(tempHeader);
					while(!(line = buffreader.readLine()).equalsIgnoreCase("EOG")){
						String token[]=line.split(" ");
						String  name = "";
						for(int i =1;i<token.length;i++){

							name+=token[i];
							name+=" ";
							Log.d("Parag====",name);
						}
						tempMap.put(token[0],name);
						PhoneNamesNumbers.add(name);
						numMapping.put(name,token[0]);
					}
					listDataChild.put(tempHeader, PhoneNamesNumbers );
				}
			}catch (Exception e) 
			{
				e.printStackTrace();
			}
			instream.close();

		}
		catch (Exception e) 
		{
			Log.d("Parag",e.getMessage());
		}
		listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
		expListView.setAdapter(listAdapter);
		
		expListView.setOnChildClickListener( new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
                     
				/*finalNum.add(numMapping.get(parent.getExpandableListAdapter()
						.getChild(groupPosition, childPosition).toString()));
				v.setBackgroundColor(Color.YELLOW);*/
				return false;
			}
		});

		composeGroupMessage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				System.out.println(PhoneNumbers.size());
			    System.out.println(!PhoneNumbers.equals("[]"));
			
				if(PhoneNumbers.size()==0){
					Toast.makeText(getApplicationContext(), "Please select atleast one group", Toast.LENGTH_SHORT).show();
				}
				else{
					System.out.println("inside else");
					Intent i = new Intent(getApplicationContext(),ComposeText.class);
				i.putStringArrayListExtra("PhoneNumbers",
						(ArrayList<String>) PhoneNumbers);
				startActivity(i);
				}
			}
		});
		
		/*selectAll.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});*/

		
	}

}