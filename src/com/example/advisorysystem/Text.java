package com.example.advisorysystem;

import com.example.adapter.TabsPagerAdapter;







import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

/*
 * Author-UB
 * Text Activity contains three tabs that contain inbox, outbox and sent fragments for text messages. 
 * ViewPager is defined here to create swiping tabs.
 * Compose button is present which redirects to the compose activity.
 */
public class Text extends ActionBarActivity {

	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = { "Inbox", "Outbox", "Sent" };
	ImageButton compose;
	SharedPreferences sharedPref;
	boolean isPublisher;


	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.tabslayout);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setTitle("Text Messages");
		compose = (ImageButton) findViewById(R.id.imageButton1);
		viewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		isPublisher = Boolean.valueOf((sharedPref.getString("publisher", "false")));
		viewPager.setAdapter(mAdapter);

		
		//compose text message button
		compose.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i;
				if(isPublisher){
					i = new Intent(getApplicationContext(), PublisherCompose.class);
					startActivity(i);
				}
				else{
					i = new Intent(getApplicationContext(), ComposeText.class);
					i.putExtra("GroupName", DashBoard.ClubSpinner.getSelectedItem().toString());
					startActivity(i);
				}
			}
		});

		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabReselected(Tab arg0,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTabSelected(Tab tab,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				viewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab arg0,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
			}
		};
		
		//setting up tabs
		for (int i = 0; i < 3; i++) {
			actionBar.addTab(
					actionBar.newTab()
					.setText(tabs[i])
					.setTabListener((android.support.v7.app.ActionBar.TabListener) tabListener));
		}

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// When swiping between pages, select the
				// corresponding tab.
				getSupportActionBar().setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id == R.id.home){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}