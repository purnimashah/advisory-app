package com.example.advisorysystem;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar.Tab;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.example.adapter.TabsPagerAdapter;
import com.example.adapter.TabsPagerAdapterAudio;

/*
 * Author-UB
 * Audio Activity holds the IVRS call function and Call recording facility 
 * for all the calls made through the app.
*/

public class Audio extends ActionBarActivity {
	
	private ViewPager viewPager;
	private TabsPagerAdapterAudio mAdapter;
	private ActionBar actionBar;
	private String[] tabs = { "Record", "Sent" };
	
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tabslayoutaudio);
		View mView= findViewById(R.id.audio_tab_activity); 
		mView.setBackgroundColor(Color.parseColor("#FFFFFF"));
		ActionBar actionBar = getSupportActionBar();
		actionBar.setTitle("Audio Messages");
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new TabsPagerAdapterAudio(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {
		
			@Override
			public void onTabReselected(Tab arg0,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTabSelected(Tab tab,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				viewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab arg0,
					android.support.v4.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
			}
		};

		for (int i = 0; i < 2; i++) {
			actionBar.addTab(
					actionBar.newTab()
					.setText(tabs[i])
					.setTabListener((android.support.v7.app.ActionBar.TabListener) tabListener));
		}

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// When swiping between pages, select the
				// corresponding tab.
				getSupportActionBar().setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id == R.id.home){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}