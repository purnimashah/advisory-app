package com.example.advisorysystem;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.widget.AdapterViewCompat;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;


/*
 * 	Author-UB
 * 	Dashboard provides a spinner to select the groups. 
 * 	Based upon the selection, the text message, audio and video activity works.
 *  Help activity provides general guidelines to use the app.
 */
public class DashBoard extends ActionBarActivity implements OnItemSelectedListener,OnClickListener {

	static Spinner ClubSpinner;

	ImageButton Text, Audio,Video, Help;
	Button Login;
	String[] state = {"AFC"};
	String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IVRS/myGroups.txt";
	List<String> myGroups;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		View mView= findViewById(R.id.main_activity); 
		mView.setBackgroundColor(Color.parseColor("#E0E0E0"));
		myGroups = new ArrayList<String>();

		try
		{
			//Read all the groups from the file

			FileInputStream instream = new FileInputStream(path); 
			InputStreamReader inputreader = new InputStreamReader(instream); 
			BufferedReader buffreader = new BufferedReader(inputreader); 
			String line = "";
			try
			{
				while ((line = buffreader.readLine()) != null)
				{				
					myGroups.add(line);
				}
			}catch (Exception e) 
			{
				e.printStackTrace();
			}
			instream.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		Text = (ImageButton)findViewById(R.id.text);
		Audio = (ImageButton)findViewById(R.id.audio);
		Video = (ImageButton)findViewById(R.id.videos);
		Help = (ImageButton)findViewById(R.id.helps);

		
		String finalMyGroup[]= new String[myGroups.size()];
		for(int i=0;i<finalMyGroup.length;i++){
			finalMyGroup[i]=myGroups.get(i);
		}

		ArrayAdapter<String> adapter_state;
		ClubSpinner = (Spinner) findViewById(R.id.clubSpinner);
		if(MainActivity.isPublisher){
			//setting spinner value to groupname if member is a publisher
			adapter_state = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, state);
		}
		else{
			//setting spinner value to many values in case of member
			adapter_state = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, finalMyGroup);
		}
		adapter_state
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ClubSpinner.setAdapter(adapter_state);

		ClubSpinner.setOnItemSelectedListener(this);
		Text.setOnClickListener(this);
		Audio.setOnClickListener(this);
		Video.setOnClickListener(this);
		Help.setOnClickListener(this);

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i;

		if(v==Text){
			i = new Intent(this, Text.class);
			startActivity(i);
		}
		if(v==Audio){
			i = new Intent(this, Audio.class);
			startActivity(i);
		}
		if(v==Video){
			i = new Intent(this, Video.class);
			Toast.makeText(getApplicationContext(), "Video Classification yet to be done", Toast.LENGTH_SHORT).show();
		}

		if(v==Help){
			Toast.makeText(getApplicationContext(), "Yet to be Updated", Toast.LENGTH_SHORT).show();
		}
	}
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		ClubSpinner.setSelection(position);
		String selState = (String) ClubSpinner.getSelectedItem();
	}
	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		//This intent will take you to mainactivity and then finish() will be called from MainActivity.
		
		Intent launchNextActivity;
		launchNextActivity = new Intent(this, MainActivity.class);
		launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);                  
		launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		launchNextActivity.putExtra("EXIT", true);
		startActivity(launchNextActivity);
	}

}