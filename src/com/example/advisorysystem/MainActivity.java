package com.example.advisorysystem;

import java.util.List;
import java.util.Set;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;


/*
 * Author - UB
 * The execution starts here.
 * At the very first startup a login screen is implemented.
 * On successful login the user can then get the to the Dashboard.
 * 
 */


public class MainActivity extends ActionBarActivity  {


	EditText phoneNumber;	
	LoginVerification loginVerification; // Asynctask
	static boolean isPublisher;
	boolean registered;
	Handler handler;	//Asynctask and main thread message passing system

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent().getBooleanExtra("EXIT", false)) {
			/*
			 * Perform exit when back button is pressed in dashboard activity
			 * puts the app in background
			 */
			finish();
		}
		else{


			final SharedPreferences sharedPref = PreferenceManager.
					getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = sharedPref.edit();
			registered = sharedPref.getBoolean("registered", false);	//returns true if registered value is present in shared Preference
			isPublisher = Boolean.valueOf(sharedPref.getString("publisher", "false"));	//returns the value of publisher if value is present in shared Preference
			handler = new Handler(){
				@Override
				public void handleMessage(Message msg) {

					if(msg.obj.toString().equalsIgnoreCase("false")){
						//Handler Message is false i.e. Not registered.
						Toast.makeText(getApplicationContext(), "You are not a registered Member.", Toast.LENGTH_LONG).show();
					}
					else{

						//User is registered.
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putBoolean("registered", true);
						
						editor.commit();
						//Starting Dashboard Activity.
						Intent i = new Intent(MainActivity.this, DashBoard.class);
						startActivity(i);
					}
				}
			};

			if(registered){
				//if already registered
				Intent i = new Intent(this, DashBoard.class);
				startActivity(i);
			}
			else
			{
				//new user
				setContentView(R.layout.login);
				phoneNumber = (EditText)findViewById(R.id.phoneNumber);

			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		return super.onOptionsItemSelected(item);
	}


	public void login(View v){
		// Method associated with onclick of Login button in res/layout/login.xml

		String number = phoneNumber.getText().toString();
		if(number.equalsIgnoreCase("")){
			//checking if number entered is ""
			Toast.makeText(getApplicationContext(), "Enter Valid number", Toast.LENGTH_SHORT).show();

		}
		else {
			long i = Long.parseLong(number);
			long length = (long)(Math.log10(i)+1);
			if(length<10 ){
				//checking the length of number
				Toast.makeText(getApplicationContext(), "Enter Valid number", Toast.LENGTH_SHORT).show();
			}
			else{
				//The number length is 10 digits... checking the validity of the number.
				loginVerification = new LoginVerification(MainActivity.this, handler);
				loginVerification.execute(number);
			}
		}
	}


}