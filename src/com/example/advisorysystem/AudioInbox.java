package com.example.advisorysystem;



import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.method.DialerKeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Author-UB
 * AudioInbox is designed to make calls to the IVRS system.
 * These calls are recorded and stored locally for the future reference of the user.
 * 
 */

public class AudioInbox extends Fragment {
	ImageButton record;
	Button recordViaData;
	TelephonyManager telManager;
	boolean recordStarted;
	MediaRecorder mrec;
	TextView callInfo;
	static Handler handler;
	AudioManager audioManager; 


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.recordaudio, container, false);
		record = (ImageButton)rootView.findViewById(R.id.record);
		recordViaData = (Button)rootView.findViewById(R.id.recordViaData);
		callInfo = (TextView)rootView.findViewById(R.id.callInfo);
		callInfo.setText("Press to call " +(String)DashBoard.ClubSpinner.getSelectedItem());
		audioManager = (AudioManager)getActivity().getSystemService(Context.AUDIO_SERVICE);
		record.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				//Call the IVRS.
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:02030157370" ));
				startActivity(intent);

				//Call Recording.
				String fileName = "sound-"+System.currentTimeMillis();
				mrec = new MediaRecorder();
				mrec.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
				mrec.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				mrec.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
				mrec.setOutputFile( Environment.getExternalStorageDirectory().getAbsolutePath()+"/IVRS/"+fileName+".3gp");
				telManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
				telManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
				audioManager.setMode(AudioManager.MODE_IN_CALL);
				audioManager.setSpeakerphoneOn(true);

			}
		});

		recordViaData.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
	            builder1.setMessage("Write your message here.");
	            builder1.setCancelable(false);
	            
	            LayoutInflater inflater1 = (LayoutInflater)getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater1.inflate(R.layout.recordviadata, (ViewGroup)getActivity().findViewById(R.id.root_element));
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(layout);
				final AlertDialog alertDialog = builder.create();
	            
			}
		});
		
		return rootView;
	}
	
	

	//Listen to the phonestate to stop the recording when phone state changes to idle.
	private final PhoneStateListener phoneListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			try {
				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING: {
					break;
				}
				case TelephonyManager.CALL_STATE_OFFHOOK: {

					handler = new Handler(){

						@Override
						public void handleMessage(Message msg) {
							// TODO Auto-generated method stub
							super.handleMessage(msg);
							try {
								mrec.prepare();
								mrec.start();
								recordStarted = true;
								Toast.makeText(getActivity(), "Recording Started", Toast.LENGTH_SHORT).show();
								Log.d("Parag","Recording Started");
							} catch (IllegalStateException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					};
					break;
				}
				case TelephonyManager.CALL_STATE_IDLE: {
					if (recordStarted) {
						mrec.stop();
						recordStarted = false;
						audioManager.setSpeakerphoneOn(false);
					}
					break;
				}
				default: { }
				}
			} catch (Exception ex) {

			}
		}
	};
}
