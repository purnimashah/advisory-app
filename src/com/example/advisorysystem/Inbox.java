package com.example.advisorysystem;



import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.text.Html;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/*
 * Author-UB
 * Shows the inbox fragment that displays the inbox messages from the content provider.
 * 
*/
public class Inbox extends Fragment {

	ListView listview;
	SimpleCursorAdapter adapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.inbox, container, false);
		ListView list = (ListView) rootView.findViewById(R.id.inboxlistview);
		List<String> msgList = getSMS();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, msgList);
	    list.setAdapter(adapter);
		return rootView;
	}

	public List<String> getSMS() {
		List<String> card = new ArrayList<String>();
		Uri uriSMSURI = Uri.parse("content://sms/inbox");
		Cursor cur = getActivity().getContentResolver().query(uriSMSURI, null, null, null,null);
		int i=0;
		while (cur.moveToNext()) {

			String address = cur.getString(cur.getColumnIndex("address"));
			String body = cur.getString(cur.getColumnIndexOrThrow("body"));
			card.add("Number:" + address + "\nMessage:\n" + body);
			
		}
		return card;
	}


}
