package com.example.advisorysystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.DropBoxManager.Entry;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class ComposeText extends ActionBarActivity{


	EditText receiver,message_body;
	Button send;
	boolean isPublisher=false;
	List<String> phoneNumberList = null;
	SendGroupMessage sendMessage ;
	boolean selectUser=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.compose);
		getSupportActionBar().setTitle("Compose");
		View mView= findViewById(R.id.compose_activity); 
		mView.setBackgroundColor(Color.parseColor("#E3F2FD"));
		send = (Button)findViewById(R.id.send);
		String str = (String)DashBoard.ClubSpinner.getSelectedItem();
		Log.d("Parag",str);
		receiver = (EditText)findViewById(R.id.receiver);
		receiver.setMaxLines(5);
		message_body = (EditText)findViewById(R.id.message_body);
		Bundle extras = getIntent().getExtras();

		if(extras.getStringArrayList("PhoneNumbers") != null){
			isPublisher= true;
			if(extras.getStringArrayList("PhoneNumbers").toString().equalsIgnoreCase("[]")){
				receiver.setText("Please select contacts");
			}else{
				receiver.setText(extras.getStringArrayList("PhoneNumbers").toString());
				phoneNumberList = extras.getStringArrayList("PhoneNumbers");
				selectUser=true;
			}
		}

		if(extras.getString("GroupName")!=null){
			receiver.setText(extras.getString("GroupName"));
		}


		send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(isPublisher){
					if(selectUser){
						for(int i =0;i<phoneNumberList.size();i++){
							HttpPost post = new HttpPost("http://www.kookoo.in/outbound/outbound_sms.php");

							//"phone_no="+number+"&api_key=KK4063496a1784f9f003768bd6e34c185b&unicode=true&message="+message+"&callback=?"
							try{
								List<NameValuePair> pairs = new ArrayList<NameValuePair>();
								pairs.add(new BasicNameValuePair("api_key", "KK4063496a1784f9f003768bd6e34c185b"));
								pairs.add(new BasicNameValuePair("unicode","true"));
								pairs.add(new BasicNameValuePair("senderid","AFCDIR"));
								pairs.add(new BasicNameValuePair("phone_no", phoneNumberList.get(i)));
								if(message_body.getText().toString().equals("")){
									Toast.makeText(getApplicationContext(), "Please write something message",Toast.LENGTH_SHORT).show();

								}
								else{
								pairs.add(new BasicNameValuePair("message", message_body.getText().toString()));
								pairs.add(new BasicNameValuePair("callback", "?"));
								post.setEntity(new UrlEncodedFormEntity(pairs));
								sendMessage = new SendGroupMessage();
								sendMessage.execute(post);
								}
							

							} catch (Exception e) {
								Log.e("Parag", e.toString());
							}

						}
						Toast.makeText(getApplicationContext(), "Message Sent to all",Toast.LENGTH_SHORT).show();
					}
					else{
						Toast.makeText(getApplicationContext(), "Select people to send message",Toast.LENGTH_SHORT).show();
					}
				}else{
					if(message_body.equals("")){
						Toast.makeText(getApplicationContext(), "Cant send empty Text", Toast.LENGTH_SHORT).show();
					}else{
						try {
							// Get the default instance of the SmsManager
							SmsManager smsManager = SmsManager.getDefault();
							if(message_body.getText().toString().equals("")){
								Toast.makeText(getApplicationContext(), "Please write something in message body",
										Toast.LENGTH_LONG).show();
							}
							else {
							smsManager.sendTextMessage("09227507512",null,"IIT-B "+message_body.getText().toString(),null,	null);
							Toast.makeText(getApplicationContext(), "Your sms has successfully sent!",
									Toast.LENGTH_LONG).show();
							}
						} catch (Exception ex) {
							Toast.makeText(getApplicationContext(),"Your sms has failed...",Toast.LENGTH_SHORT).show();
							ex.printStackTrace();
						}
					}
				}
			}
		});
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		/*int id = item.getItemId();
		if(id == R.id.home){
			return true;
		}*/

		return super.onOptionsItemSelected(item);
	}

	public class SendGroupMessage extends AsyncTask<HttpPost, Void, Integer> {

		protected Integer doInBackground(HttpPost... posts) {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = posts[0];
			HttpResponse response;
			try {
				response = client.execute(post);
				//response = null; // for testing failure of sending code

			} catch (Exception e) {
				Log.e("Parag", e.toString());
				return 500; // error
			}
			if (response != null){
				Log.d("Parag",""+response.getStatusLine().getStatusCode());
				return response.getStatusLine().getStatusCode();
			}
			else{
				Log.d("Parag","500");
				return 500; // error
			}
		}
	}
}