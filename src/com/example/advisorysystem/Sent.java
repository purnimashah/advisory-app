package com.example.advisorysystem;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Sent extends Fragment {

	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.sent, container, false);
	        rootView.setBackgroundColor(Color.parseColor("#EDE7F6"));
	        ListView list = (ListView) rootView.findViewById(R.id.sentlistview);
		    List<String> msgList = getSMS();
		    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, msgList);
		    list.setAdapter(adapter);
			return rootView;
		}
		
		public List<String> getSMS() {
		    List<String> sms = new ArrayList<String>();
		    Uri uriSMSURI = Uri.parse("content://sms/sent");
		    Cursor cur = getActivity().getContentResolver().query(uriSMSURI, null, null, null,null);
		    while (cur.moveToNext()) {
		        String address = cur.getString(cur.getColumnIndex("address"));
		        String body = cur.getString(cur.getColumnIndexOrThrow("body"));
		        
		        sms.add("Number:" + address + "\nMessage:\n" + body);
		    }
		    return sms;
		}
	        
	}

