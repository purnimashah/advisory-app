package com.example.advisorysystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AudioSent extends Fragment {

	private File file;
	private static final String MEDIA_PATH = new String(
			Environment.getExternalStorageDirectory() +"/IVRS/");
	MediaPlayer mediaPlayer;
	Button stop;
	SeekBar seekBar;
	TextView nowPlaying;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.audiosent, container, false);
		rootView.setBackgroundColor(Color.parseColor("#F8BBD0"));
		ListView listView = (ListView)rootView.findViewById(R.id.audiolist);
		file = new File(MEDIA_PATH);
		if (!file.exists()) {
			file.mkdir();
			Log.d("Parag","Audio File created");

		}
		
		
		File list[] = file.listFiles();
		
		if(list == null){
			Toast.makeText(getActivity(), "File list is empty", Toast.LENGTH_SHORT).show();
			return rootView;
		}
		else{
			List<String> myList = new ArrayList<String>();
			for (int i = 0; i < list.length; i++) {
				myList.add(list[i].getName());
			}

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_list_item_1, myList);
			adapter.notifyDataSetChanged();
			listView.setAdapter(adapter);
			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			listView.setCacheColorHint(Color.TRANSPARENT);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					String value = (String)parent.getItemAtPosition(position);

					Dialog yourDialog = new Dialog(view.getContext());
					LayoutInflater inflater1 = (LayoutInflater)getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View layout = inflater1.inflate(R.layout.seekbardialog, (ViewGroup)getActivity().findViewById(R.id.your_dialog_root_element));
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(layout);
					final AlertDialog alertDialog = builder.create();

					nowPlaying = (TextView)layout.findViewById(R.id.nowPlaying);
					stop = (Button)layout.findViewById(R.id.stopButton); 
					seekBar = (SeekBar)layout.findViewById(R.id.seekBar);
					mediaPlayer = MediaPlayer.create(getActivity(),Uri.fromFile(new File(MEDIA_PATH+"/"+value)));
					seekBar.setMax(mediaPlayer.getDuration());
					alertDialog.setCancelable(false);
					alertDialog.show();
					mediaPlayer.start();  

					nowPlaying.setText("Now Playing - " + value);
					stop.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							mediaPlayer.stop();
							alertDialog.dismiss();
						}
					});

					final Handler mHandler = new Handler();

					Runnable mRunnable = new Runnable() {

						@Override
						public void run() {

							seekBar.setProgress(mediaPlayer.getCurrentPosition());
							mHandler.postDelayed(this, 1000);
						}
					};

					/*seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

						@Override
						public void onStopTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onStartTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {                
							if(mediaPlayer != null && fromUser){
								mediaPlayer.seekTo(progress * 1000);
							}
						}
					});*/
					/*
					try {
						//mediaPlayer = new MediaPlayer();

						mediaPlayer.setDataSource(MEDIA_PATH+"/"+value);
						//seekBar.setMax(mediaPlayer.getDuration());
						mediaPlayer.prepare();  

					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					 */					
				}

			});
			return rootView;
		}
	}
}

