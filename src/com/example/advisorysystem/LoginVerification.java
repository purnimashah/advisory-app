package com.example.advisorysystem;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.util.Log;
import android.widget.Toast;

/*
 * Author- UB
 * LoginVerification is an asynctask that does the following:
 * 1. Check if the number is a member
 * 2. If a member then the groups in which he is present is put in an array.
 * 3. Check if the member is a publisher
 * 4. If a publisher then add a shared Pref and get the member list of the group.
 */
public class LoginVerification extends AsyncTask<String, String, Boolean>{

	ProgressDialog pd;
	Context context;
	InputStream is;
	Handler handler;
	HashMap<String, Map<String,String>> groupMembers = null; 
	String checkNumberAPI, getAllGroupsAPI, isPublisherAPI;
	JSONArray memberOfGroups;
	List<String> groups,stringGroups,StringGroups;
	Boolean isPublisher = null;
	Map<Integer, String> groupsMap = null;
	JsonParser jParser;
	JSONObject checkNumber,getAllGroups,checkPublisher;



	public LoginVerification(Context context, Handler handler) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.handler = handler;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		//Setting up the wait Dialog box
		pd = new ProgressDialog(context);
		pd.setMessage("Wait");
		//pd.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {

		//Checking number is present in the Database.

		checkNumberAPI = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=memberGroups&number=" + params[0];
		jParser = new JsonParser();
		JSONObject checkNumber = jParser.getJSONFromUrl(checkNumberAPI);
		//

		try {
			if(checkNumber.get("groups").toString().equals("[]")){
						//Empty array means user not registered.
				pd.dismiss();
				return false;
			}

			else{

				//Get All Groups
				getAllGroupsAPI = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=allGroups";
				getAllGroups = jParser.getJSONFromUrl(getAllGroupsAPI);

				//Get group list from JsonObject.
				try {
					groupsMap = jParser.toMap(getAllGroups.getJSONObject("groups"));//add group name and number into groupsMap

				}catch (JSONException e) {
					e.printStackTrace();
				}

				//Check if given number is publisher
				isPublisherAPI = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=checkPublisher&number="+params[0];
				checkPublisher = jParser.getJSONFromUrl(isPublisherAPI);
				isPublisher = Boolean.valueOf(checkPublisher.get("publisher").toString());


				groups = new ArrayList<String>();
				stringGroups = new ArrayList<String>();
				//returns all the groups [54,1,10]
				memberOfGroups= checkNumber.getJSONArray("groups");

				for(int i=0;i<memberOfGroups.length(); i++){
					//Putting JSONArray into a arraylist
					groups.add(memberOfGroups.getString(i));
				}

				for(int i=0;i<groups.size();i++){
					if(groupsMap.containsKey(groups.get(i)));

					//Getting corresponding Strings from the group numbers
					stringGroups.add(groupsMap.get(groups.get(i)));
				}
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		//Writing groups to file for the retrieval in the spinner on Dashboard
		writeGroupsToFile();	

		if(isPublisher){
			//If the member is a publisher then the list of all the members gets downloaded.
			publisherProcessing();
		}

		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub

		Message msg = Message.obtain();
		msg.obj = result.toString();
		handler.sendMessage(msg);
		pd.dismiss();

	}

	protected void writeGroupsToFile(){

		File mygroups = new File(
				Environment.getExternalStorageDirectory().getAbsolutePath()+"/IVRS/myGroups.txt");
		try {

			BufferedWriter writer = new BufferedWriter(new FileWriter(mygroups,false));
			for ( int i = 0; i < stringGroups.size(); i++)
			{      
				writer.write(stringGroups.get(i) + " \n");
			}
			writer.close();
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}

	protected void publisherProcessing(){

		final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("publisher", "true");
		editor.commit();

		String getMemberOfGroupAPI;
		JSONObject getMembers;
		Map<String,String> tempMap ;
		groupMembers = new HashMap<String, Map<String,String>>();
		for (Entry<Integer, String> entry : groupsMap.entrySet())
		{				
			getMemberOfGroupAPI = "http://ruralivrs.cse.iitb.ac.in/AFC/IvrsServlet?req_type=getMembers&groupId="+entry.getKey();
			getMembers = jParser.getJSONFromUrl(getMemberOfGroupAPI);
			try {
				tempMap = JsonParser.toMap(getMembers.getJSONObject("members"));
				groupMembers.put(entry.getValue(), tempMap);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		/*
		 *  Writing all the Members into a file.
		 * 	The format will be :
		 * 	Group Name:
		 * 	Member1 Number1
		 * 	Member2 Number2
		 * 	EOG(End of Group)
		 */

		File membersList = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/IVRS/MembersList.txt");
		try {

			BufferedWriter writer = new BufferedWriter(new FileWriter(membersList,false));
			Map<String,String> tempMap1 = new HashMap<String,String>();
			for (Entry<String, Map<String,String>> entry : groupMembers.entrySet())
			{
				tempMap1=entry.getValue();
				writer.write(entry.getKey()+"\n");
				for(Entry<String, String> entry1 : tempMap1.entrySet()){
					writer.write(entry1.getKey()+" " +entry1.getValue()+"\n");
				}
				writer.write("EOG\n");
			}

			writer.close();
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}


}
